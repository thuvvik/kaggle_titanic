datas to understand : https://www.kaggle.com/c/titanic/data

** Markdown
* https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
* https://daringfireball.net/projects/markdown/


** Titanic
* https://www.kaggle.com/poonaml/titanic-survival-prediction-end-to-end-ml-pipeline
* https://www.kaggle.com/arthurtok/introduction-to-ensembling-stacking-in-python
* https://www.kaggle.com/sinakhorami/titanic-best-working-classifier
* https://www.kaggle.com/manuelc/best-model-mixed-classifier
* https://www.kaggle.com/omarelgabry/a-journey-through-titanic
* https://www.kaggle.com/startupsci/titanic-data-science-solutions
