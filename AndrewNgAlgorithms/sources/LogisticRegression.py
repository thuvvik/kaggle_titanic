from scipy.special import expit
import numpy as np
import scipy.optimize as opt

# to test : from scipy.optimize import fmin_bfgs
#           fmin_bfgs(decorated_cost, initial_theta, maxiter=400))


class LogisticRegression:

    @staticmethod
    def sigmoid(z):
        return expit(z)

    @staticmethod
    def cost(theta, x, y, lambdaregul, regularized=False, debug=False):
        if theta.ndim == 1:
            theta = theta.reshape(-1, 1)
        theta = np.matrix(theta)
        x = np.matrix(x)
        y = np.matrix(y)
        m, n = x.shape
        if debug:
            print('cost shapes : (x, y ,theta, hypothesis)')
            print(x.shape)
            print(y.shape)
            print(theta.shape)

        hypothesis = LogisticRegression.sigmoid(x * theta)
        if debug:
            print(hypothesis.shape)

        for_y_1 = np.multiply(-y, np.log(hypothesis))
        for_y_0 = np.multiply((1 - y), np.log(1 - hypothesis))
        costvalue = (1/m) * (for_y_1 - for_y_0).sum()

        regularizationpart = lambdaregul / (2 * m) * np.power(theta[1:n, :], 2).sum()
        if regularized:
            costvalue = costvalue + regularizationpart

        return costvalue

    @staticmethod
    def gradient(theta, x, y, lambdaregul, regularized=False, debug=False):
        if theta.ndim == 1:
            theta = theta.reshape(-1, 1)
        theta = np.matrix(theta)
        x = np.matrix(x)
        y = np.matrix(y)
        m, n = x.shape
        if debug:
            print('gradient shapes : (x, y ,theta)')
            print(x.shape)
            print(y.shape)
            print(theta.shape)

        hypothesis = LogisticRegression.sigmoid(x * theta)
        error = hypothesis - y
        grad = (x.T * error) / m

        thetaminuszero = theta.copy()
        thetaminuszero[0, :] = 0
        regularizationpart = (lambdaregul/m) * thetaminuszero

        if debug:
            print(grad.shape)
            print(regularizationpart.shape)

        if regularized:
            grad = grad + regularizationpart

        return grad  # .flatten()

    @staticmethod
    def minimize(x, y, theta, lambdaregul=1, withregularization=False, debug=False):
        if not LogisticRegression.control_shapes(x, y, theta):
            return

        if debug:
            print('Cout avec theta initial:')
            print("%.17f" % LogisticRegression.cost(theta, x, y, lambdaregul, withregularization, debug))

        myargs = (x, y, lambdaregul, withregularization, debug)
        result = opt.minimize(fun=LogisticRegression.cost, x0=theta, jac=LogisticRegression.gradient, args=myargs
                              , method='TNC'
                              , options={'disp': debug})

        if debug:
            print('Cout avec theta final (apres regression)')
            print("%.17f" % LogisticRegression.cost(result[0], x, y, lambdaregul, withregularization, debug))

        return result

    @staticmethod
    def control_shapes(x, y, theta):
        dimensions_problem_message = 'WARNING %s isn\'t a 2-dimensions array'
        if x.ndim != 2:
            print(dimensions_problem_message % 'x')
            return False

        if y.ndim != 2:
            print(dimensions_problem_message % 'y')
            return False

        if theta.ndim != 2:
            print(dimensions_problem_message % 'theta')
            return False

        mx, nx = x.shape
        my, ny = y.shape
        mt, nt = theta.shape

        if nx != mt:
            print('Problem between x number of columns(features) and theta numbers of rows (features gradients) ')
            print('x shape: %s ' % (x.shape,))
            print('theta shape: %s ' % (theta.shape,))
            return False

        if mx != my:
            print('Problem between x numbers of rows (training examples number) and y numbers of rows (labels number)')
            print('x shape: %s ' % (x.shape,))
            print('y shape: %s ' % (y.shape,))
            return False

        if ny != 1 or nt != 1:
            print('y and theta should have only 1 column')
            print('y shape: %s ' % (y.shape,))
            print('theta shape: %s ' % (theta.shape,))
            return False

        return True

    @staticmethod
    def predict(theta, x):
        theta = np.matrix(theta)
        x = np.matrix(x)

        prediction = x * theta

        prediction = np.sum(prediction, 1)  # sum along rows
        prediction[prediction >= 0] = 1
        prediction[prediction < 0] = 0

        return prediction

    @staticmethod
    def measure_prediction_accuracy(theta, x, y):
        theta = np.matrix(theta)
        predictions = LogisticRegression.predict(theta, x)
        accuracy = np.mean((predictions == y) * 100)
        return accuracy

