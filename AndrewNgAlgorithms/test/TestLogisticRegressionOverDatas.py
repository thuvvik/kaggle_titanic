from unittest import TestCase
from ..sources.LogisticRegression import LogisticRegression as lr
import numpy as np
import pandas as pd
from .TestsHelpers import TestsHelpers
import os


class TestLogisticRegressionOverDatas(TestCase):

    def prepare_exercise_1(self):
        filepath = os.path.join(os.path.dirname(os.path.abspath(__file__)), "datas/ex2data1.txt")
        df_datas_ex1 = pd.read_csv(filepath, header=None, names=['Exam 1', 'Exam 2', 'Admitted'])
        df_datas_ex1.insert(0, 'Ones', 1)

        cols = df_datas_ex1.shape[1]
        x = np.array(df_datas_ex1.iloc[:, 0:(cols - 1)])
        y = np.array(df_datas_ex1.iloc[:, (cols - 1):cols])
        theta = np.zeros(x.shape[1]).reshape(-1, 1)
        regularized = False
        lambdaregul = 0

        return x, y, theta, regularized, lambdaregul

    def test_exerciseNoReg_1_AndrewNg(self):
        x, y, theta, regularized, lambdaregul = self.prepare_exercise_1()

        actualcost = lr.cost(theta, x, y, lambdaregul, regularized)
        expectedcost = 0.693
        self.assertAlmostEqual(expectedcost, actualcost, delta=0.001)

        expectedgrad = np.matrix([[-0.1000], [-12.0092], [-11.2628]])
        actualgrad = lr.gradient(theta, x, y, lambdaregul, regularized)
        TestsHelpers.helper_control_gradientvalues(expectedgrad, actualgrad, theta, 0.0001)

    def test_exerciseNoReg_1_test_AndrewNg(self):
        x, y, theta, regularized, lambdaregul = self.prepare_exercise_1()
        theta = np.matrix([[-24], [0.2], [0.2]])

        actualcost = lr.cost(theta, x, y, lambdaregul, regularized)
        expectedcost = 0.218
        self.assertAlmostEqual(expectedcost, actualcost, delta=0.001)

        expectedgrad = np.matrix([[0.043], [2.566], [2.647]])
        actualgrad = lr.gradient(theta, x, y, lambdaregul, regularized)
        TestsHelpers.helper_control_gradientvalues(expectedgrad, actualgrad, theta, 0.001)

    def test_exerciseNoReg_1_minimized_AndrewNg(self):
        x, y, theta, regularized, lambdaregul = self.prepare_exercise_1()

        result = lr.minimize(x, y, theta, lambdaregul, regularized)
        minimizedtheta = result['x'].reshape(-1, 1)

        actualcost = lr.cost(minimizedtheta, x, y, lambdaregul, regularized)
        expectedcost = 0.203
        self.assertAlmostEqual(expectedcost, actualcost, delta=0.001)

        expectedtheta = np.matrix([[-25.161], [0.206], [0.201]])
        TestsHelpers.helper_control_matricesvalues(expectedtheta, minimizedtheta, 0.001)

        # For a student with scores 45 and 85, we predict an admission probability 0.775 +/- 0.002
        expectedprobability = 0.775
        studentscore = np.matrix([[1, 45, 85]])
        actualprobability = lr.sigmoid(studentscore * minimizedtheta).item(0)
        self.assertAlmostEqual(expectedprobability, actualprobability, delta=0.002)

        # control accuracy on training set
        expectedaccuracy = 89
        actualaccuracy = lr.measure_prediction_accuracy(minimizedtheta, x, y)
        self.assertAlmostEqual(expectedaccuracy, actualaccuracy, delta=0.1)

    def prepare_exercise_2(self):
        filepath = os.path.join(os.path.dirname(os.path.abspath(__file__)), "datas/ex2data2.txt")
        df_datas_ex2 = pd.read_csv(filepath, header=None, names=['Microchip Test 1', 'Microchip Test 2', 'Accepted'])
        x1 = df_datas_ex2['Microchip Test 1']
        x2 = df_datas_ex2['Microchip Test 2']

        df_datas_ex2 = TestsHelpers.mapfeature(x1, x2, df_datas_ex2)

        df_datas_ex2.drop('Microchip Test 1', axis=1, inplace=True)
        df_datas_ex2.drop('Microchip Test 2', axis=1, inplace=True)

        cols = df_datas_ex2.shape[1]
        x = np.array(df_datas_ex2.iloc[:, 1:cols])
        y = np.array(df_datas_ex2.iloc[:, 0].reshape(-1, 1))
        theta = np.zeros(x.shape[1]).reshape(-1, 1)
        regularized = True
        lambdaregul = 1

        return x, y, theta, regularized, lambdaregul

    def test_exerciseReg_2(self):
        x, y, theta, regularized, lambdaregul = self.prepare_exercise_2()

        actualcost = lr.cost(theta, x, y, lambdaregul, regularized)
        expectedcost = 0.693
        self.assertAlmostEqual(expectedcost, actualcost, delta=0.001)

        expectedgrad = np.matrix([[0.0085], [0.0188], [0.0001], [0.0503], [0.0115]])
        actualgrad = lr.gradient(theta, x, y, lambdaregul, regularized)
        TestsHelpers.helper_control_gradientvalues(expectedgrad, actualgrad, theta, 0.0001)

    def test_exerciseReg_2_theta_as_ones(self):
        x, y, theta, regularized, lambdaregul = self.prepare_exercise_2()
        theta = np.ones(x.shape[1]).reshape(-1, 1)
        lambdaregul = 10

        actualcost = lr.cost(theta, x, y, lambdaregul, regularized)
        expectedcost = 3.16
        self.assertAlmostEqual(expectedcost, actualcost, delta=0.01)

        expectedgrad = np.matrix([[0.3460], [0.1614], [0.1948], [0.2269], [0.0922]])
        actualgrad = lr.gradient(theta, x, y, lambdaregul, regularized)
        TestsHelpers.helper_control_gradientvalues(expectedgrad, actualgrad, theta, 0.0001)

    def test_exerciseReg_2_accuracy(self):
        x, y, theta, regularized, lambdaregul = self.prepare_exercise_2()

        result = lr.minimize(x, y, theta, lambdaregul, regularized)
        minimizedtheta = result['x'].reshape(-1, 1)

        # control accuracy on training set
        expectedaccuracy = 83.1
        actualaccuracy = lr.measure_prediction_accuracy(minimizedtheta, x, y)
        self.assertAlmostEqual(expectedaccuracy, actualaccuracy, delta=0.1)
