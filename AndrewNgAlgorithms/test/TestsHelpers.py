import unittest
import numpy as np


class TestsHelpers:

    @staticmethod
    def helper_control_gradientvalues(expectedgrad, actualgrad, theta, delta=0.00001):
        tc = unittest.TestCase('__init__')
        tc.assertEqual(theta.shape, actualgrad.shape)
        TestsHelpers.helper_control_matricesvalues(expectedgrad, actualgrad, delta=delta)

    @staticmethod
    def helper_control_matricesvalues( expectedmatrix, actualmatrix, delta=0.00001):
        tc = unittest.TestCase('__init__')
        for i in range(0, expectedmatrix.shape[0]):
            tc.assertAlmostEqual(expectedmatrix.item(i), actualmatrix.item(i), delta=delta)

    @staticmethod
    def mapfeature(x1, x2, out, degree=6):
        # MAPFEATURE Feature mapping function to polynomial features
        #
        #   MAPFEATURE(X1, X2) maps the two input features
        #   to quadratic features used in the regularization exercise.
        #
        #   Returns a new feature array with more features, comprising of
        #   X1, X2, X1.^2, X2.^2, X1*X2, X1*X2.^2, etc..
        #   for a total of 1 + 2 + ... + (degree+1) = ((degree+1) * (degree+2)) / 2 columns
        #
        #   Inputs X1, X2 must be the same size
        #

        curr_column = 1
        out[curr_column] = np.ones(x1.shape[0])
        curr_column += 1
        for i in range(1, degree + 1):
            for j in range(0, i + 1):
                out[curr_column] = np.power(x1, i - j) * np.power(x2, j)
                curr_column += 1

        return out