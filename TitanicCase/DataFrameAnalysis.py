import numpy as np
import pandas as pd
from pandas.util.terminal import get_terminal_size

# ignore warnings
import warnings
warnings.filterwarnings('ignore')
pd.set_option('display.width', get_terminal_size()[0])


class DataFrameAnalysis:
    """ Class represents an analysis performed on any data frame
    based on this insights :

    https://www.kaggle.com/jeru666/memory-reduction-and-data-insights/notebook
    """

    def __init__(self, dataframe):
        """
        We store the data frame we want to analyze
        :param dataframe: to be analyzed
        """
        self.dataframe = dataframe

    def performinitialanalysis(self):

        # --- Displays memory consumed by each column ---
        print("****************************************")
        print("Displays memory consumed by each column")
        print(self.dataframe.memory_usage())
        print("\n")

        # --- Displays memory consumed by entire dataframe ---
        print("****************************************")
        print("Displays memory consumed by entire dataframe")
        mem = self.dataframe.memory_usage(index=True).sum()
        print(mem / 1024 ** 2, " MB")
        print("\n")

        # --- Check whether it has any missing values ----
        print("****************************************")
        print("Check whether it has any missing values")
        print(self.dataframe.isnull().sum())
        print("\n")

        # --- check which columns have Nan values ---
        print("****************************************")
        print("check which columns have Nan values")
        print(self.dataframe.columns[self.dataframe.isnull().any()].tolist())
        print("\n")

        # --- Check the datatypes of each of the columns in the dataframe ---
        print("****************************************")
        print("Check the datatypes of each of the columns in the dataframe")
        print(self.dataframe.dtypes)
        print("\n")

        for column in self.dataframe.columns:
            # --- Min/max on column to reduce datatype memory print ---

            try:
                maxval = np.max(self.dataframe[column])
                minval = np.min(self.dataframe[column])
                print("****************************************")
                print("Min/max on column %s to reduce datatype memory print" % column)
                print(maxval)
                print(minval)
            except TypeError:
                print("****************************************")
                print("Can't find Min/max on column %s " % column)

        # --- Bring some insight on dataframe by displaying some lines ---
        print("****************************************")
        print("Bring some insight on dataframe by displaying some lines")
        print(self.dataframe.sample(5))
        print("\n")

        # --- See a summary of the dataframe ---
        print("****************************************")
        print("See a summary of the dataframe")
        print(self.dataframe.describe(include="all"))
        print("\n")

