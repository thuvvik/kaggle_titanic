from unittest import TestCase
from ..sources.LogisticRegression import LogisticRegression as lr
import numpy as np
from .TestsHelpers import TestsHelpers


class TestLogisticRegression(TestCase):
    def test_sigmoid(self):
        """
        https://www.coursera.org/learn/machine-learning/discussions/all/threads/JHorKeknEeS0tyIAC9RBcw
        :return:
        """
        self.assertAlmostEqual(0.0066929, lr.sigmoid(-5), places=7)
        self.assertAlmostEqual(0.50000, lr.sigmoid(0), places=1)
        self.assertAlmostEqual(0.99331, lr.sigmoid(5), places=5)

    def test_sigmoid_vector(self):
        """
        https://www.coursera.org/learn/machine-learning/discussions/all/threads/JHorKeknEeS0tyIAC9RBcw
        :return:
        """

        # first vector test case
        initial = np.array([4, 5, 6])
        expected = np.array([0.98201, 0.99331, 0.99753])

        actual = lr.sigmoid(initial)
        np.testing.assert_array_almost_equal(expected, actual, 5)

        # second vector test case
        initial = np.array([-1, 0, 1])
        expected = np.array([0.26894, 0.50000, 0.73106])

        actual = lr.sigmoid(initial)
        np.testing.assert_array_almost_equal(expected, actual, 5)

    def test_sigmoid_matrix(self):
        """
        https://www.coursera.org/learn/machine-learning/discussions/all/threads/JHorKeknEeS0tyIAC9RBcw
        :return:
        """
        # octave =>  reshape(-1:.1:.9, 4, 5);
        initialmatrix = np.matrix([[-1.00000, -0.60000, -0.20000, 0.20000, 0.60000]
                                      , [-0.90000, -0.50000, -0.10000, 0.30000, 0.70000]
                                      , [-0.80000, -0.40000, 0.00000, 0.40000, 0.80000]
                                      , [-0.70000, -0.30000, 0.10000, 0.50000, 0.90000]])
        expected = np.matrix([[0.26894, 0.35434, 0.45017, 0.54983, 0.64566]
                                 , [0.28905, 0.37754, 0.47502, 0.57444, 0.66819]
                                 , [0.31003, 0.40131, 0.50000, 0.59869, 0.68997]
                                 , [0.33181, 0.42556, 0.52498, 0.62246, 0.71095]])

        actual = lr.sigmoid(initialmatrix)
        np.testing.assert_array_almost_equal(expected, actual, 5)

    def test_costFunction_lambda0(self):
        """
        https://www.coursera.org/learn/machine-learning/discussions/weeks/3/threads/tA3ESpq0EeW70BJZtLVfGQ

        :return: assert True
        """
        x = np.matrix([[1, 8, 1, 6]
                       , [1, 3, 5, 7]
                       , [1, 4, 9, 2]])
        y = np.matrix([[1], [0], [1]])
        theta = np.matrix([[-2], [-1], [1], [2]])
        regularized = False
        lambdaregul = 0

        actualcost = lr.cost(theta, x, y, lambdaregul, regularized)

        expectedcost = 4.6832
        self.assertAlmostEqual(expectedcost, actualcost, delta=0.0001)

        expectedgrad = np.matrix([[0.31722], [0.87232], [1.64812], [2.23787]])
        actualgrad = lr.gradient(theta, x, y, lambdaregul, regularized)
        TestsHelpers.helper_control_gradientvalues(expectedgrad, actualgrad, theta, 0.00001)

    def test_costFunction_lambda4(self):
        """
        https://www.coursera.org/learn/machine-learning/discussions/weeks/3/threads/tA3ESpq0EeW70BJZtLVfGQ

        :return: assert True
        """
        x = np.matrix([[1, 8, 1, 6]
                       , [1, 3, 5, 7]
                       , [1, 4, 9, 2]])
        y = np.matrix([[1], [0], [1]])
        theta = np.matrix([[-2], [-1], [1], [2]])
        regularized = True
        lambdaregul = 4

        actualj = lr.cost(theta, x, y, lambdaregul, regularized)
        expectedj = 8.6832
        self.assertAlmostEqual(expectedj, actualj, places=4)

        expectedgrad = np.matrix([[0.31722], [-0.46102], [2.98146], [4.90454]])
        actualgrad = lr.gradient(theta, x, y, lambdaregul, regularized)
        TestsHelpers.helper_control_gradientvalues(expectedgrad, actualgrad, theta, 0.00001)
