
import pandas as pd
from TitanicCase.DataFrameAnalysis import DataFrameAnalysis
from TitanicCase.Visualizer import Visualizer
import os
import numpy as np
from AndrewNgAlgorithms.sources.LogisticRegression import LogisticRegression
from sklearn.metrics import precision_recall_fscore_support as score
from sklearn.preprocessing import PolynomialFeatures


def split_dataframe_to_trainandtest_sets(dataframe, completesetssize=10):
    from sklearn.utils import shuffle

    shuffled_df = dataframe  # .reindex(np.random.permutation(dataframe.index))  # copy made here
    nbrows = shuffled_df.shape[0]

    if completesetssize > nbrows:
        raise ValueError('complete sets size can\'t exceed the total number of rows of the dataframe')

    sixtypercent = (completesetssize // 100) * 70
    train = shuffled_df.head(sixtypercent).copy()
    test = shuffled_df.tail((completesetssize - sixtypercent)).copy()

    del shuffled_df

    return train, test


def memorycaring_trainandtest_sets_builder(dataframe, completesetssize=10):
    import multiprocessing

    with multiprocessing.Pool(1) as pool:
        train, test = pool.starmap(split_dataframe_to_trainandtest_sets, [(dataframe, completesetssize)])[0]

    return train, test


def prepareDataFrame(dataframe, with_analysis=False):
    # for a first try i'll just drop the column that I don't trust
    # PassengerId   => drop
    # Survived      => keep y
    # Pclass        => keep x
    # Name          => drop
    # Sex           => keep x
    # Age           => drop
    # SibSp         => keep x
    # Parch         => keep x
    # Ticket        => drop
    # Fare          => drop
    # Cabin         => drop
    # Embarked      => keep x
    mediane = dataframe["Age"].median()
    print(mediane)
    dataframe['Age'].fillna(mediane, inplace=True)  # .astype(float)
    mybins = list(range(0, 101, 5))
    mylabels = list(range(0, len(mybins) - 1, 1))
    dataframe['Categorized_Age'] = pd.cut(dataframe['Age'], bins=mybins, include_lowest=True, labels=mylabels).astype(
        float)

    # drop useless for now
    dataframe.drop('PassengerId', axis=1, inplace=True)
    dataframe.drop('Name', axis=1, inplace=True)
    dataframe.drop('Age', axis=1, inplace=True)
    dataframe.drop('Ticket', axis=1, inplace=True)
    dataframe.drop('Fare', axis=1, inplace=True)
    dataframe.drop('Cabin', axis=1, inplace=True)

    # enhance kept columns
    dataframe.fillna({"Embarked": "S"}, inplace=True)  # most people embarked in southampton => to demonstrate

    dataframe['Pclass'] = dataframe['Pclass'].astype(int)
    dataframe['Sex'] = dataframe['Sex'].map({'female': 0, 'male': 1}).astype(int)
    dataframe['SibSp'] = dataframe['SibSp'].astype(int)
    dataframe['Parch'] = dataframe['Parch'].astype(int)
    dataframe['Embarked'] = dataframe['Embarked'].map({'S': 0, 'C': 1, 'Q': 2}).astype(int)

    # https://www.kaggle.com/sinakhorami/titanic-best-working-classifier
    dataframe['FamilySize'] = dataframe['SibSp'] + dataframe['Parch'] + 1
    dataframe['FamilySize'] = dataframe['FamilySize'].astype(int)
    # df_titanic['IsAlone'] = 0
    # df_titanic.loc[df_titanic['FamilySize'] == 1, 'IsAlone'] = 1
    dataframe.drop('SibSp', axis=1, inplace=True)
    dataframe.drop('Parch', axis=1, inplace=True)



    if with_analysis:
        analysis = DataFrameAnalysis(dataframe)
        analysis.performinitialanalysis()

    return dataframe


def splitxy(dataset, degree):

    y = np.array(dataset['Survived'].copy()).reshape(-1, 1)
    dataset.drop('Survived', axis=1, inplace=True)

    poly = PolynomialFeatures(degree, include_bias=True)
    dataset = poly.fit_transform(dataset)
    x = np.array(dataset)

    return x, y


def trainFromDatas(trainset, degree):

    x, y = splitxy(trainset, degree)

    theta = np.zeros(x.shape[1]).reshape(-1, 1)
    regularized = True
    lambdaregul = 1

    result = LogisticRegression.minimize(x, y, theta, lambdaregul, regularized)
    minimizedtheta = result['x'].reshape(-1, 1)

    return minimizedtheta, x, y


def compute_error_toll(theta, x, y):
    m = x.shape[0]
    theta = np.matrix(theta)
    x = np.matrix(x)
    y= np.matrix(y)

    prediction = (x * theta) - y
    prediction_error = np.square(prediction)

    error_toll = np.sum(prediction_error)  # sum cells by row
    error_toll = error_toll / (2*m)

    return error_toll


# collectorCostTrainTest = [[], [], []]

# for degree in range(0, 4):
# i = 0

    # filepath = os.path.join(os.path.dirname(os.path.abspath(__file__)), "datas/train.csv")
    # df_titanic = pd.read_csv(filepath, dtype={'Age': np.float64})
    # analysis = DataFrameAnalysis(df_titanic)
    # analysis.performinitialanalysis()

    # prepareDataFrame(df_titanic)

    # vis = Visualizer(None)
    # vis.plot_correlation_map(df_titanic)

    # trainset, testset = split_dataframe_to_trainandtest_sets(df_titanic, 891)

    # trained_theta, xtrain, ytrain = trainFromDatas(trainset, degree)
    # training_error = compute_error_toll(trained_theta, xtrain, ytrain)

    # xtest, ytest = splitxy(testset, degree)
    # test_error = compute_error_toll(trained_theta, xtest, ytest)

    # y_predicted = LogisticRegression.predict(trained_theta, xtrain)
    # collectorCostTrainTest[0].append(degree)
    # collectorCostTrainTest[1].append(training_error)
    # collectorCostTrainTest[2].append(test_error)

    # control accuracy on training set
    # y_predicted = LogisticRegression.predict(minimizedtheta, x)
    # actualaccuracy = LogisticRegression.measure_prediction_accuracy(minimizedtheta, x, y)
    # precision, recall, fscore, support = score(y, y_predicted, average='binary')

    # ytest_predicted = LogisticRegression.predict(minimizedtheta, xtest)
    # actualtestaccuracy = LogisticRegression.measure_prediction_accuracy(minimizedtheta, xtest, ytest)
    # print('accuracy on test set : %s ' % actualtestaccuracy)

    # precisiontest, recalltest, fscoretest, supporttest = score(ytest, ytest_predicted, average='binary')
    # i += 1
    # if i % 100 == 0:
    #     print('round %s' % i)

    # if precisiontest >= currenttestprecision and precision >= currenttrainprecision\
    #         and fscoretest >= currenttestfscore and fscore >= currenttrainfscore:
    #     print('BEEETTEEEER !!!! ')
    #     currenttrainprecision = precision
    #     currenttestprecision = precisiontest
    #     currenttrainfscore = fscore
    #     currenttestfscore = fscoretest

    #     print('precision: %.7f // precisiontest %.7f' % (precision, precisiontest))
    #     print('fscore: %.7f // fscoretest %.7f' % (fscore, fscore))

    #     foundtheta = minimizedtheta.copy()
    #     # print(foundtheta)

    #     print('accuracy on training set : %s ' % actualaccuracy)
    #     print('precision: {}'.format(precision))
    #     print('recall: {}'.format(recall))
    #     print('fscore: {}'.format(fscore))
    #     print('support: {}'.format(support))

    #     print('accuracy on training set : %s ' % actualtestaccuracy)
    #     print('precision: {}'.format(precisiontest))
    #     print('recall: {}'.format(recalltest))
    #     print('fscore: {}'.format(fscoretest))
    #     print('support: {}'.format(supporttest))

# print(collectorCostTrainTest)
# vis = Visualizer(None)
# vis.plot(collectorCostTrainTest[0], collectorCostTrainTest[1], collectorCostTrainTest[2])
# vis.plot_correlation_map(df_titanic)
# time to rank .. somewhere on the ladder

# filepath = os.path.join(os.path.dirname(os.path.abspath(__file__)), "datas/test.csv")
# df_proof = pd.read_csv(filepath, dtype={'Age': np.float64, 'PassengerId': np.int16})
# passengerIds = np.array(df_proof['PassengerId'].copy()).reshape(-1, 1)

# prepareDataFrame(df_proof)
# poly = PolynomialFeatures(degree, include_bias=True)
# df_proof = poly.fit_transform(df_proof)
# xproof = np.array(df_proof)

# yproof_predicted = LogisticRegression.predict(foundtheta, xproof)
# submission = np.hstack((passengerIds, yproof_predicted))

# submission[0] = submission[0].astype(int)

# filepath = os.path.join(os.path.dirname(os.path.abspath(__file__)), "datas/submission.csv")
# np.savetxt(filepath, submission, fmt='%i', delimiter=",", header='PassengerId, Survived')


filepath = os.path.join(os.path.dirname(os.path.abspath(__file__)), "datas/train.csv")
df_titanic = pd.read_csv(filepath, dtype={'Age': np.float64})

prepareDataFrame(df_titanic)

vis = Visualizer(None)
vis.plot_correlation_map(df_titanic)










