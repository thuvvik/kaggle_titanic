import matplotlib
# ['GTK3Agg', 'gdk', 'TkAgg', 'GTKAgg', 'GTK3Cairo', 'MacOSX', 'emf', 'nbAgg', 'GTKCairo', 'pgf', 'GTK', 'WX', 'cairo', 'CocoaAgg', 'template', 'ps', 'svg', 'agg', 'WebAgg', 'WXAgg', 'Qt5Agg', 'pdf', 'Qt4Agg']


import matplotlib.pyplot as plt
plt.interactive(False)
import numpy as np
import pandas as pd
import seaborn as sns
sns.set()


# ignore warnings
import warnings
warnings.filterwarnings('ignore')

class Visualizer:
    """
    Building a Visualizer helper, for a panda's DataFrame
    """
    def __init__(self, dataframe):
        """
        The default constructor stores the dataframe for later use

        :param dataframe: datas to visualize
        :type dataframe: DataFrame
        """
        self.dataframe = dataframe
        self.ax = 1
        self.fig = 1

    def show_scatter_plot(self, distinction, respectto_x, respectto_y):
        """
        creates a scatter plot of the distinction column
        with respect to part1 and part2 columns

        Args:
        :param distinction: the column to show distinctively on the plot
        :type distinction: str
        :param respectto_x: The column on axis X along which distinction is spread
        :type respectto_x: str
        :param respectto_y: The column on axis X along which distinction is spread
        :type respectto_y: str
        """

        positive_samples = self.dataframe[self.dataframe[distinction].isin([1])]
        negative_samples = self.dataframe[self.dataframe[distinction].isin([0])]

        self.fig, self.ax = plt.subplots(figsize=(12, 8))
        self.ax.scatter(positive_samples[respectto_x], positive_samples[respectto_y]
                   , s=50, c='blue', marker='o', label=distinction)
        self.ax.scatter(negative_samples[respectto_x], negative_samples[respectto_y]
                   , s=50, c='red', marker='x', label=('Not %s' % distinction))
        self.ax.legend()
        title = '%s given %s score and %s score' % (distinction, respectto_x, respectto_y)
        self.fig.suptitle(title, fontsize=18, fontweight='bold')
        self.ax.set_xlabel(('%s Score' % respectto_x))
        self.ax.set_ylabel(('%s Score' % respectto_y))



    def add_decision_boundary(self, x, y, theta_trained):
        plt.contour(x, y, theta_trained)
        plt.show()

    @staticmethod
    def show_function_on(func, xlow=-10, xhigh=10, step=1, functionname=''):
        nums = np.arange(xlow, xhigh, step)

        fig, ax = plt.subplots(figsize=(12, 8))
        ax.plot(nums, func(nums), 'red')
        fig.suptitle(functionname, fontsize=18, fontweight='bold')
        plt.show()

    @staticmethod
    def plotDecisionBoundary(theta, X, y):
        # PLOTDECISIONBOUNDARY Plots the data points X and y into a new figure with
        # the decision boundary defined by theta
        #   PLOTDECISIONBOUNDARY(theta, X,y) plots the data points with + for the
        #   positive examples and o for the negative examples. X is assumed to be
        #   a either
        #   1) Mx3 matrix, where the first column is an all-ones column for the
        #      intercept.
        #   2) MxN, N>3 matrix, where the first column is all-ones

        import matplotlib.pyplot as plt

        # Plot Data
        fig = plt.figure()

        plt, p1, p2 = Visualizer.plotData(X[:, 1:3], y)

        plt.hold(True)

        if X.shape[1] <= 3:
            # Only need 2 points to define a line, so choose two endpoints
            plot_x = np.array([min(X[:, 1]) - 2, max(X[:, 1]) + 2])

            # Calculate the decision boundary line
            plot_y = (-1. / theta[2]) * (theta[1] * plot_x + theta[0])

            # Plot, and adjust axes for better viewing
            p3 = plt.plot(plot_x, plot_y)

            # Legend, specific for the exercise
            plt.legend((p1, p2, p3[0]), ('Admitted', 'Not Admitted', 'Decision Boundary'), numpoints=1,
                       handlelength=0.5)

            plt.axis([30, 100, 30, 100])

            plt.show(block=False)
        else:
            # Here is the grid range
            u = np.linspace(-1, 1.5, 50)
            v = np.linspace(-1, 1.5, 50)

            nbfeatures = sum(range(8))
            z = np.zeros((X.shape[0], nbfeatures))
            # Evaluate z = theta*x over the grid
            for i in range(1, len(u)+1):
                for j in range(1, len(v)+1):
                    mf = Visualizer.mapfeature(np.array([u[i]]), np.array([v[j]]), z)
                    z[i, j] = np.dot(mf, theta)

            z = np.transpose(z)  # important to transpose z before calling contour

            # Plot z = 0
            # Notice you need to specify the level 0
            # we get collections[0] so that we can display a legend properly
            p3 = plt.contour(u, v, z, levels=[0], linewidth=2).collections[0]

            # Legend, specific for the exercise
            plt.legend((p1, p2, p3), ('y = 1', 'y = 0', 'Decision Boundary'), numpoints=1, handlelength=0)

            plt.show(block=False)

        plt.hold(False)  # prevents further drawing on plot



    @staticmethod
    def plotData(X, y):
        # PLOTDATA Plots the data points X and y into a new figure
        #   PLOTDATA(x,y) plots the data points with + for the positive examples
        #   and o for the negative examples. X is assumed to be a Mx2 matrix.

        # ====================== YOUR CODE HERE ======================
        # Instructions: Plot the positive and negative examples on a
        #               2D plot, using the option 'k+' for the positive
        #               examples and 'ko' for the negative examples.
        #

        # Find Indices of Positive and Negative Examples
        pos = np.where(y == 1)
        neg = np.where(y == 0)

        # plot! [0] indexing at end necessary for proper legend creation in ex2.py
        p1 = plt.plot(X[pos, 0], X[pos, 1], marker='+', markersize=9, color='k')[0]
        p2 = plt.plot(X[neg, 0], X[neg, 1], marker='o', markersize=7, color='y')[0]

        return plt, p1, p2

    @staticmethod
    def plot( datas):
        # plt.figure()
        # plt.hist(datas)
        # 

        # draw a bar plot of survival by sex
        for column in datas.columns:
            print('for column : %s' % column)
            sns.barplot(x=column, y="Survived", data=datas)
            plt.show()

    @staticmethod
    def plot(degree_serie, trainerror_serie, testerror_serie):
        plt.plot(degree_serie, trainerror_serie, 'r', degree_serie, testerror_serie, 'b')
        plt.show()

    @staticmethod
    def plot_correlation_map(df):

        import time

        corr = df.corr()
        _, ax = plt.subplots(figsize=(12, 10))
        colormap = plt.get_cmap('viridis')
        _ = sns.heatmap(
            corr,
            cmap=colormap,
            square=True,
            linewidths=0.1,
            vmax=1.0,
            annot=True,
            linecolor='white',
        )
        for t in ax.texts:
            print(t.get_text())
        plt.yticks(rotation=0)
        plt.show(block=True)
